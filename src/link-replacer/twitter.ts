import { ReplacedLink } from './link-replacer';
import * as fetch from 'node-fetch';

export const twitterReplacer = async (
  text: string,
): Promise<ReplacedLink[]> => {
  const matches = text.matchAll(
    /((?:https?:)?\/\/)?((?:www|m|mobile)\.)?((?:twitter\.com))\/(\S+)/gm,
  );
  const replacedLinks: ReplacedLink[] = [];
  const nitterInstance = await findNitterInstance();
  for (const match of matches) {
    replacedLinks.push({
      old: match[0],
      new: nitterInstance + '/' + match[4].split('?')[0],
    });
  }
  return replacedLinks;
};

async function findNitterInstance(): Promise<string> {
  const response = await fetch(
    'https://raw.githubusercontent.com/xnaas/nitter-instances/master/history/summary.json',
    {
      method: 'GET',
    },
  );
  const instances = await response.json();
  return instances[0].url;
}
