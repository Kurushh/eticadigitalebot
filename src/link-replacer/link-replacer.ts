import { Composer } from 'grammy';
import { CustomContext } from 'src/bot';
import { config } from '../config';
import { getUserTag, escapeText } from '../utils';
import { twitterReplacer } from './twitter';
import { youtubeReplacer } from './youtube';

export type ReplacedLink = { old: string; new: string };
export const linkReplacer = new Composer<CustomContext>();

linkReplacer.on('msg', async (ctx, next) => {
  let replacedLinks: ReplacedLink[] = [];
  let text = ctx.message.text ?? ctx.message.caption;

  replacedLinks = replacedLinks.concat(await youtubeReplacer(text));
  replacedLinks = replacedLinks.concat(await twitterReplacer(text));

  if (replacedLinks.length >= 1) {
    const originalLinks: string[] = [];
    for (const link of replacedLinks) {
      text = text.replace(link.old, link.new);
      originalLinks.push(link.old);
    }

    text = `Blip blop, ho convertito il messaggio di ${getUserTag(
      ctx.message.from,
    )} in modo da rispettare la tua privacy:\n"${escapeText(
      text,
    )}"\n\n\\.\\.\\.ma se vuoi comunque aprire i link originali:${escapeText(
      originalLinks.join('\n'),
    )}\n\n[Scopri cos'è successo](${config.blipBlopExplanationLink})`;
    ctx.reply(text, { parse_mode: 'MarkdownV2' });
  }

  await next();
});
