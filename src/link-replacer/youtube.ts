// regex found at https://stackoverflow.com/a/37704433
import { ReplacedLink } from './link-replacer';
import * as fetch from 'node-fetch'

export const youtubeReplacer = async (
  text: string,
): Promise<ReplacedLink[]> => {
  const matches = text.matchAll(
    /((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?/gm,
  );
  const replacedLinks: ReplacedLink[] = [];
  const invidiousInstance = await findInvidiousInstance();
  for (const match of matches) {
    replacedLinks.push({
      old: match[0],
      new: invidiousInstance + match[5] + '?quality=dash',
    });
  }
  return replacedLinks;
};

async function findInvidiousInstance(): Promise<string> {
  const response = await fetch(
    'https://api.invidious.io/instances.json?sort_by=health',
    {
      method: 'GET',
    },
  );
  const instances = await response.json();
  return instances[0][1].uri;
}
