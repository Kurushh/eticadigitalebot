import { CustomContext } from 'src/bot';
import { User } from '../user';
import { getUserTag } from '../utils';
import { getRepository } from 'typeorm';
import { handleAdminCommand } from './admin-command-handler';

export const perdonaCommand = async (ctx: CustomContext) => {
  handleAdminCommand(ctx, async (user, reason) => {
    const userRepository = getRepository(User);
    const userFound = await userRepository.findOne({ id: user.id });
    if (!userFound) {
      ctx.reply(`L'utente ${getUserTag(user)} non è presente nel database`);
      return;
    }
    userFound.warnings = 0;
    userFound.lastWarn = null;
    userRepository.save(userFound);
    await ctx.restrictChatMember(user.id, {
      can_send_polls: true,
      can_change_info: true,
      can_invite_users: true,
      can_pin_messages: true,
      can_send_messages: true,
      can_send_media_messages: true,
      can_send_other_messages: true,
      can_add_web_page_previews: true,
    });
    await ctx.unbanChatMember(user.id, { only_if_banned: true });
    if (!reason) {
      ctx.reply(`L'utente ${getUserTag(user)} è stato perdonato\\.`, {
        parse_mode: 'MarkdownV2',
      });
    } else {
      ctx.reply(
        `L'utente ${getUserTag(user)} è stato perdonato\\.\nMotivo: ${reason}`,
        { parse_mode: 'MarkdownV2' },
      );
    }
  });
};
