import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryColumn()
  id: number;

  @Column({ nullable: true })
  username?: string;

  @Column({ default: 0 })
  warnings: number;

  @Column({ nullable: true })
  lastWarn?: number;
}
